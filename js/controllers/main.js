"use strict";

/**
 * @ngdoc function
 * @name App.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the App
 */

angular.module("app")

    .controller("MainCtrl",function($http, $scope, $filter, $location, SmoothScroll){

        $scope.options = [1,1,1,2,2];
        $scope.page = 1;

        $scope.goTo = function (id){
            SmoothScroll.scrollTo(id);
        };

        $scope.tco = [];
        $scope.details = [];

        $scope.setPage = function(page){
            $scope.page = page;
        };

        $http.get("resource/tco.json").success(function(data){
            $scope.tco = data;
            $http.get("resource/cost_detail.json").success(function(data){
                $scope.details = data;
                $scope.calcTCO = function(data){
                    return $filter("filter")(data,function(item){return item.combination == $scope.options.join(",")});
                };

                $scope.fiveYear = $scope.calcTCO($scope.tco);
                $scope.costDetail = $scope.calcTCO($scope.details);

                $scope.columnChart = {
                    type: "ColumnChart",
                    data: {
                        cols: [
                            {
                                id: "tco",
                                type: "string"
                            },
                            {
                                id: "value",
                                type: "number",
                                label: "5 Year Total Costs by TCO"
                            },
                            {
                                id: "color",
                                type: "string",
                                role: "style"
                            },
                            {
                                id: "label",
                                type: "number",
                                role: "annotation"
                            }
                        ],
                        rows: [
                            {
                                c: [
                                    { v: "IaaS"},
                                    { v: $scope.fiveYear[0].five_year_total},
                                    { v: "#B4E573"},
                                    { v: $filter("currency")($scope.fiveYear[0].five_year_total,"$",0)}
                                ]
                            },
                            {
                                c: [
                                    { v: "Managed Srv"},
                                    { v: $scope.fiveYear[1].five_year_total},
                                    { v: "#77CBE8"},
                                    { v: $filter("currency")($scope.fiveYear[1].five_year_total,"$",0)}
                                ]
                            },
                            {
                                c: [
                                    { v: "Co-Location"},
                                    { v: $scope.fiveYear[2].five_year_total},
                                    { v: "#0095DA"},
                                    { v: $filter("currency")($scope.fiveYear[2].five_year_total,"$",0)}
                                ]
                            }
                        ]
                    },
                    options: {
                        legend: "left",
                        width: 500,
                        height: 450,
                        fill: 50,
                        displayExactValues: true,
                        vAxis: {
                            format: "$ ###,###,###,###",
                            gridlines: {
                                "count": 5
                            }
                        },
                        hAxis: {
                            textPosition: "top"
                        }
                    },
                    displayed: true
                };

                $scope.areaChart = {
                    type: "AreaChart",
                    data: {
                        cols: [
                            {
                                id: "period",
                                type: "string"
                            },
                            {
                                id: "iaas",
                                type: "number",
                                label: "IaaS"
                            },
                            {
                                id: "mgd_service",
                                type: "number",
                                label: "Managed Srv"
                            }
                            ,{
                                id: "co_location",
                                type: "number",
                                label: "Co-Location"
                            }
                        ],
                        rows: [
                            {
                                c: [
                                    { v: "One Time"},
                                    { v: $scope.fiveYear[0].one_time},
                                    { v: $scope.fiveYear[1].one_time},
                                    { v: $scope.fiveYear[2].one_time}
                                ]
                            },
                            {
                                c: [
                                    { v: "Year 1"},
                                    { v: $scope.fiveYear[0].year_1},
                                    { v: $scope.fiveYear[1].year_1},
                                    { v: $scope.fiveYear[2].year_1}
                                ]
                            },
                            {
                                c: [
                                    { v: "Year 2"},
                                    { v: $scope.fiveYear[0].year_2},
                                    { v: $scope.fiveYear[1].year_2},
                                    { v: $scope.fiveYear[2].year_2}
                                ]
                            },
                            {
                                c: [
                                    { v: "Year 3"},
                                    { v: $scope.fiveYear[0].year_3},
                                    { v: $scope.fiveYear[1].year_3},
                                    { v: $scope.fiveYear[2].year_3}
                                ]
                            },
                            {
                                c: [
                                    { v: "Year 4"},
                                    { v: $scope.fiveYear[0].year_4},
                                    { v: $scope.fiveYear[1].year_4},
                                    { v: $scope.fiveYear[2].year_4}
                                ]
                            },
                            {
                                c: [
                                    { v: "Year 5"},
                                    { v: $scope.fiveYear[0].year_5},
                                    { v: $scope.fiveYear[1].year_5},
                                    { v: $scope.fiveYear[2].year_5}
                                ]
                            }
                        ]
                    },
                    options: {
                        width: 500,
                        height: 450,
                        fill: 20,
                        displayExactValues: true,
                        vAxis: {
                            format: "$ ###,###,###,###",
                            cssStyles:"color:#B2B2B2",
                            gridlines: {
                                count: 10,
                                options: {
                                    color:"#ff0000"
                                }
                            }
                        },
                        hAxis: {},
                        series: {
                            0: { color: '#B4E573' },
                            1: { color: '#77CBE8' },
                            2: { color: '#0095DA' }
                        }
                    },
                    displayed: true
                };

                $scope.updateCharts = function(){

                    $scope.fiveYear = $scope.calcTCO($scope.tco);
                    $scope.costDetail = $scope.calcTCO($scope.details);

                    $scope.columnChart.data.rows = [
                        {
                            c: [
                                { v: "IaaS"},
                                { v: $scope.fiveYear[0].five_year_total},
                                { v: "#B4E573"},
                                { v: $filter("currency")($scope.fiveYear[0].five_year_total,"$",0)}
                            ]
                        },
                        {
                            c: [
                                { v: "Managed Srv"},
                                { v: $scope.fiveYear[1].five_year_total},
                                { v: "#77CBE8"},
                                { v: $filter("currency")($scope.fiveYear[1].five_year_total,"$",0)}
                            ]
                        },
                        {
                            c: [
                                { v: "Co-Location"},
                                { v: $scope.fiveYear[2].five_year_total},
                                { v: "#0095DA"},
                                { v: $filter("currency")($scope.fiveYear[2].five_year_total,"$",0)}
                            ]
                        }
                    ];

                    $scope.areaChart.data.rows = [
                        {
                            c: [
                                { v: "One Time"},
                                { v: $scope.fiveYear[0].one_time},
                                { v: $scope.fiveYear[1].one_time},
                                { v: $scope.fiveYear[2].one_time}
                            ]
                        },
                        {
                            c: [
                                { v: "Year 1"},
                                { v: $scope.fiveYear[0].year_1},
                                { v: $scope.fiveYear[1].year_1},
                                { v: $scope.fiveYear[2].year_1}
                            ]
                        },
                        {
                            c: [
                                { v: "Year 2"},
                                { v: $scope.fiveYear[0].year_2},
                                { v: $scope.fiveYear[1].year_2},
                                { v: $scope.fiveYear[2].year_2}
                            ]
                        },
                        {
                            c: [
                                { v: "Year 3"},
                                { v: $scope.fiveYear[0].year_3},
                                { v: $scope.fiveYear[1].year_3},
                                { v: $scope.fiveYear[2].year_3}
                            ]
                        },
                        {
                            c: [
                                { v: "Year 4"},
                                { v: $scope.fiveYear[0].year_4},
                                { v: $scope.fiveYear[1].year_4},
                                { v: $scope.fiveYear[2].year_4}
                            ]
                        },
                        {
                            c: [
                                { v: "Year 5"},
                                { v: $scope.fiveYear[0].year_5},
                                { v: $scope.fiveYear[1].year_5},
                                { v: $scope.fiveYear[2].year_5}
                            ]
                        }
                    ];

                };
            });
        });

        $scope.questions = [
            {
                title: "1. Size of Infrastructure",
                description: "What size is the product will you be serving up to your customers?",
                options: [
                    {
                        label: "Small",
                        class: "option1_1",
                        value: 1
                    },
                    {
                        label: "Medium",
                        class: "option1_2",
                        value: 2
                    },
                    {
                        label: "Large",
                        class: "option1_3",
                        value: 3
                    }
                ]
            },
            {
                title: "2. Complexity of Infrastructure",
                description: "How complex is your product’s functionality or the amount of data that will be processed?",
                options: [
                    {
                        label: "Simple",
                        class: "option2_1",
                        value: 1
                    },
                    {
                        label: "Moderate",
                        class: "option2_2",
                        value: 2
                    },
                    {
                        label: "Very",
                        class: "option2_3",
                        value: 3
                    }
                ]
            },
            {
                title: "3. Age of Infrastructure",
                description: "Is your product an existing single tenant solution, or a more modern application developed on a multi-tenant platform?",
                options: [
                    {
                        label: "Single-Tenant",
                        class: "option3_1",
                        value: 1
                    },
                    {
                        label: "Mixed",
                        class: "option3_2",
                        value: 2
                    },
                    {
                        label: "Multi-Tenant",
                        class: "option3_3",
                        value: 3
                    }
                ]
            },
            {
                title: "4. Global Operations",
                description: "Is your product being consumed by customers on three or more continents?",
                options: [
                    {
                        label: "Yes",
                        class: "optionB_1",
                        value: 1
                    },
                    {
                        label: "Global Operations",
                        class: "icon",
                        value: "img/globe.png"
                    },
                    {
                        label: "No",
                        class: "optionB_2",
                        value: 2
                    }
                ]
            },
            {
                title: "5. Scalability",
                description: "Will this product require large bursts of computing capacity and higher bandwidth to support a superior customer experience?",
                options: [
                    {
                        label: "Yes",
                        class: "optionB_1",
                        value: 1
                    },
                    {
                        label: "Scalability",
                        class: "icon",
                        value: "img/scale.png"
                    },
                    {
                        label: "No",
                        class: "optionB_2",
                        value: 2
                    }
                ]
            }
        ];

    });